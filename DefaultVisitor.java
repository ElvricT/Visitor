package m87;
import java.util.*;
public abstract class DefaultVisitor implements Visitor{

	@Override
	public void visitFile(File pFile) {
	
		
	}

	@Override
	public void visitSymLink(SymLink pSymLink) {
		Iterator<IFile> aFiles = pSymLink.iterator();
		while(aFiles.hasNext())
		{
			aFiles.next().accept(this);
		}
		
	}

	@Override
	public void visitDirectory(Directory pDirectory) {
//		Iterator<IFile> aFiles = pDirectory.iterator();
//		while(aFiles.hasNext())
//		{
//			aFiles.next().accept(this);
//		}
		for(IFile a : pDirectory.aFiles)
		{
			a.accept(this);
		}
	}

}
