package m87;

public interface IFile {
	public String getName();
	public void accept(Visitor pVisitor);
}

