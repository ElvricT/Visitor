package m87;
import java.util.*;

public class Directory extends AbstractFile implements Iterable<IFile>{
	protected List<IFile> aFiles = new ArrayList<>();
	protected Directory(String pName) {
		super(pName);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void accept(Visitor pVisitor) {
		// TODO Auto-generated method stub
		pVisitor.visitDirectory(this);
	}

	@Override
	public Iterator<IFile> iterator() {
		return aFiles.iterator();
	}
	
	public void addFile(IFile pFile)
	{
		aFiles.add(pFile);
	}
	public void removeFile(IFile pFile)
	{
		aFiles.remove(pFile);
	}


}
