package m87;
import java.util.*;
public class DeleteVisitor extends DefaultVisitor{
	String aName;
	public DeleteVisitor(String pName)
	{
		aName=pName;
	}
	@Override
	public void visitFile(File pFile) {
		// TODO Auto-generated method stub
		if(pFile.getName().equals(aName))
		{
			pFile=null;
		}
		else
		{
			super.visitFile(pFile);
		}
	}

	@Override
	public void visitSymLink(SymLink pSymLink) {
		if(pSymLink.getName().equals(aName))
		{
			pSymLink.aFiles= new ArrayList<>();
		}
		else
		{
			super.visitSymLink(pSymLink);
		}
		
		
	}

	@Override
	public void visitDirectory(Directory pDirectory) {
		if(pDirectory.getName().equals(aName))
		{
			pDirectory.aFiles= new ArrayList<>();
		}
		else
		{
			Iterator<IFile> f = pDirectory.iterator();
			while(f.hasNext())
			{
				if(f.next().getName().equals(aName))
				{
					f.remove();
				}
			}
			super.visitDirectory(pDirectory);
		}
		
	}

	@Override
	public void visitHDirectory(HiddenDirectory pDirectory) {
		if(pDirectory.getName().equals(aName))
		{
			pDirectory.getDirectory().aFiles= new ArrayList<>();
		}
		else
		{
			super.visitDirectory(pDirectory.getDirectory());
		}
	}

}
