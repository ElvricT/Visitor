package m87;

public class HiddenDirectory extends AbstractFile{
	private Directory aDirectory;
	protected HiddenDirectory(Directory pDirectory) {
		super(pDirectory.getName());
		aDirectory=pDirectory;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void accept(Visitor pVisitor)
	{
		pVisitor.visitHDirectory(this);
	}
	public Directory getDirectory()
	{
		return aDirectory;
	}
	public void addFile(IFile pFile)
	{
		aDirectory.addFile(pFile);
	}
	public void removeFile(IFile pFile)
	{
		aDirectory.removeFile(pFile);
	}
}
