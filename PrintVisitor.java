package m87;

public class PrintVisitor extends DefaultVisitor{

	@Override
	public void visitFile(File pFile) {
		// TODO Auto-generated method stub
		System.out.println(pFile.getName());
		super.visitFile(pFile);
	}

	@Override
	public void visitSymLink(SymLink pSymLink) {
		System.out.println(pSymLink.getName());
		super.visitSymLink(pSymLink);
		
	}

	@Override
	public void visitDirectory(Directory pDirectory) {
		System.out.println(pDirectory.getName());
		super.visitDirectory(pDirectory);
		
	}

	@Override
	public void visitHDirectory(HiddenDirectory pDirectory) {
		System.out.println("."+ pDirectory.getName());
		super.visitDirectory(pDirectory.getDirectory());
	}

}
