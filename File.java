package m87;

public class File extends AbstractFile{

	protected File(String pName) {
		super(pName);
	}

	@Override
	public void accept(Visitor pVisitor) {
		pVisitor.visitFile(this);
	}

}
