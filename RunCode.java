package m87;

public class RunCode {
	public static void main(String[] args) {
		File aFile = new File("Swag");
		File a1File = new File("Swag1");
		File a2File = new File("Swag2");
		Directory c = new Directory("IamGone");
		Directory d = new Directory("The Big Boss");
		d.addFile(aFile);
		d.addFile(a1File);
		d.addFile(a2File);
		d.addFile(c);
		c.addFile(aFile);
		System.out.println("Start");
		d.accept(new PrintVisitor());
		d.accept(new DeleteVisitor("Swag"));
		System.out.println("Start again");
		d.accept(new PrintVisitor());
		
	}
}
