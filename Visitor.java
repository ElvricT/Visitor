package m87;

public interface Visitor {
	public void visitFile(File pFile);
	public void visitSymLink(SymLink pSymLink);
	public void visitDirectory(Directory pDirectory);
	public void visitHDirectory(HiddenDirectory pDirectory);
}
